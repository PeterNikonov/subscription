<?php

use PeterNikonov\Subscription\Condition;
use PeterNikonov\Subscription\Episode;
use PeterNikonov\Subscription\Process;
use PeterNikonov\Subscription\Contract;
use PHPUnit\Framework\TestCase;

class ProcessTest extends TestCase
{
    private function getCondition($ignoreQuantity = false, $ignoreInterval = false): Condition
    {
        $condition = new Condition();
        $condition->setIgnoreInterval($ignoreInterval);
        $condition->setIgnoreQuantity($ignoreQuantity);

        if (!$ignoreInterval) {
            $condition->setInterval(new DateInterval('P1M'));
        }

        if (!$ignoreQuantity) {
            $condition->setQuantity(10);
        }

        return $condition;
    }

    private function getEpisode(int $type, DateTime $dateTime): Episode
    {
        $episode = new Episode();
        $episode->setDatetime($dateTime);
        $episode->setType($type);

        return $episode;
    }

    private function getExampleSubscription(Condition $condition): Contract
    {
        $subscription = new Contract();
        $subscription->setCondition($condition);
        $subscription->setEpisode($this->getEpisode(Episode::START, new DateTime('2020-01-01 15:40')));
        $subscription->setEpisode($this->getEpisode(Episode::VISIT, new DateTime('2020-01-03 10:00')));

        return $subscription;
    }

    public function testProcess()
    {
        $subscription = $this->getExampleSubscription($this->getCondition(false, false));
        $process = new Process($subscription);

        $this->assertEquals('2020-02-01', $process->getTillDate()->format('Y-m-d'));
        $this->assertEquals(9, $process->getRemainingQuantity());
    }

    public function testRenewDate()
    {
        $subscription = $this->getExampleSubscription($this->getCondition(false, false));

        $renewEpisode = new Episode();
        $renewEpisode->setDatetime(new DateTime('2020-01-15 15:40'));
        $renewEpisode->setType(Episode::RENEW);
        $renewEpisode->setProlongDate(new DateTime('2020-02-01'));
        $renewEpisode->setProlongInterval(new DateInterval('P2W'));

        $subscription->setEpisode($renewEpisode);
        $process = new Process($subscription);

        $this->assertEquals('2020-02-15', $process->getRenewDate()->format('Y-m-d'));
        $this->assertEquals('2020-02-15', $process->getTillDate()->format('Y-m-d'));
    }

    public function testWithIgnoreQuantity()
    {
        $subscription = $this->getExampleSubscription($this->getCondition(true, false));

        for ($i = 0; $i < 50; $i++) {
            $subscription->setEpisode($this->getEpisode(Episode::VISIT, new DateTime('2020-01-03 10:00')));
        }

        $process = new Process($subscription);

        $this->assertNull($process->getRemainingQuantity());
        $this->assertTrue($process->isValid(new DateTime('2020-01-02')));
    }

    public function testWithIgnoreInterval()
    {
        $subscription = $this->getExampleSubscription($this->getCondition(false, true));
        $process = new Process($subscription);

        $this->assertNull($process->getTillDate());
        $this->assertTrue($process->isValid(new DateTime('2020-01-02')));
    }

    public function testValidByDate()
    {
        $subscription = new Contract();
        $subscription->setCondition($this->getCondition(false, false));
        $subscription->setEpisode($this->getEpisode(Episode::START, new DateTime('2020-01-01 15:40')));

        $process = new Process($subscription);
        $this->assertTrue($process->isValid(new DateTime('2020-01-02')));
        $this->assertFalse($process->isValid(new DateTime('2020-02-20')));
    }

    public function testValidByQuantity()
    {
        # If quantity ok
        $subscription = new Contract();
        $subscription->setCondition($this->getCondition(false, false));
        $subscription->setEpisode($this->getEpisode(Episode::START, new DateTime('2020-01-01 15:40')));

        $process = new Process($subscription);
        $this->assertTrue($process->isValid(new DateTime('2020-01-02')));

        # No remaining quantity
        $subscription = new Contract();
        $subscription->setCondition($this->getCondition(false, false));
        $subscription->setEpisode($this->getEpisode(Episode::START, new DateTime('2020-01-01 15:40')));

        for ($i = 0; $i < $subscription->getCondition()->getQuantity(); $i++) {
            $subscription->setEpisode($this->getEpisode(Episode::VISIT, new DateTime('2020-01-03 10:00')));
        }

        $process = new Process($subscription);
        $this->assertFalse($process->isValid(new DateTime('2020-01-02')));
    }
}

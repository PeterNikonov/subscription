<?php

namespace PeterNikonov\Subscription;

use DateTime;
use DateInterval;

class Episode
{
    const START = 0;
    const VISIT = 1;
    const RENEW = 2;

    protected $id;
    protected $contractId;
    protected $type;
    protected $datetime;
    protected $prolongDate;
    protected $prolongInterval;

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getType() : int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type)
    {
        $this->type = $type;
    }

    /**
     * @return DateTime
     */
    public function getDatetime() : DateTime
    {
        return $this->datetime;
    }

    /**
     * @param DateTime $datetime
     */
    public function setDatetime(DateTime $datetime)
    {
        $this->datetime = $datetime;
    }

    /**
     * @return int
     */
    public function getContractId() : int
    {
        return $this->contractId;
    }

    /**
     * @param int $contractId
     */
    public function setContractId(int $contractId)
    {
        $this->contractId = $contractId;
    }

    /**
     * @return DateTime
     */
    public function getProlongDate() : ?DateTime
    {
        return $this->prolongDate;
    }

    /**
     * @param DateTime $prolongDate
     */
    public function setProlongDate(?DateTime $prolongDate)
    {
        $this->prolongDate = $prolongDate;
    }

    /**
     * @return DateInterval
     */
    public function getProlongInterval() : ?DateInterval
    {
        return $this->prolongInterval;
    }

    /**
     * @param DateInterval $prolongInterval
     */
    public function setProlongInterval(?DateInterval $prolongInterval)
    {
        $this->prolongInterval = $prolongInterval;
    }
}

<?php

namespace PeterNikonov\Subscription;

use DateTime;
use function array_filter;
use function ksort;

class Process
{
    /**
     * @var Contract
     */
    private $subscription;

    public function __construct(Contract $subcription)
    {
        $this->subscription = $subcription;
    }

    /**
     * @return Contract
     */
    public function getSubscription(): Contract
    {
        return $this->subscription;
    }

    /**
     * @param Contract $subscription
     */
    public function setSubscription(Contract $subscription)
    {
        $this->subscription = $subscription;
    }

    /**
     * @return null|DateTime
     */
    public function getTillDate()
    {
        if ($this->subscription->getCondition()->getIgnoreInterval()) {
            return;
        }

        $interval = $this->subscription->getCondition()->getInterval();
        $startEpisode = $this->getLastEpisode($this->getEpisodesByType(Episode::START));

        if ($startEpisode) {
            $startDate = clone $startEpisode->getDatetime();
            $tillDate = $startDate->add($interval);
        }

        $renewDate = $this->getRenewDate();

        if ($renewDate AND $renewDate > $tillDate) {
            return $renewDate;
        }

        return $tillDate;
    }

    /**
     * @return bool|DateTime
     */
    public function getRenewDate()
    {
        $renewEpisode = $this->getLastEpisode($this->getEpisodesByType(Episode::RENEW));
        if ($renewEpisode) {
            $fromDate = clone $renewEpisode->getProlongDate();
            return $fromDate->add($renewEpisode->getProlongInterval());
        }
    }

    public function getRemainingQuantity()
    {
        if ($this->subscription->getCondition()->getIgnoreQuantity()) {
            return;
        }

        $nominal = $this->subscription->getCondition()->getQuantity();
        $visitEpisodes = $this->getEpisodesByType(Episode::VISIT);

        return $nominal - count($visitEpisodes);
    }

    public function isValid(DateTime $onDate = null): bool
    {
        # Quantity check
        if (!$this->subscription->getCondition()->getIgnoreQuantity()) {
            if ($this->getRemainingQuantity() <= 0) {
                return false;
            }
        }

        # Interval check
        if (!$this->subscription->getCondition()->getIgnoreInterval()) {
            $checkDate = $onDate ?? (new DateTime('now'));
            if ($checkDate > $this->getTillDate()) {
                return false;
            }
        }

        return true;
    }

    public function getEpisodesByType(int $type): array
    {
        $match = array_filter($this->subscription->getEpisodes(), function (Episode $episode) use ($type) {
            return $episode->getType() === $type;
        });

        return $match;
    }

    /**
     * @param Episode[] $episodes
     * @return void|Episode
     */
    public function getLastEpisode(array $episodes)
    {
        foreach ($episodes as $episode) {
            $timestamp = $episode->getDatetime()->getTimestamp();
            $match[$timestamp] = $episode;
        }

        if (isset($match)) {
            ksort($match);
            return array_pop($match);
        }
    }
}

<?php

namespace PeterNikonov\Subscription;

use DateInterval;

class Condition
{
    private $id;
    private $name;
    private $quantity;
    private $interval;
    private $ignoreInterval;
    private $ignoreQuantity;
    private $price;

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getQuantity() : int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return DateInterval
     */
    public function getInterval() : DateInterval
    {
        return $this->interval;
    }

    /**
     * @param DateInterval $interval
     */
    public function setInterval(DateInterval $interval)
    {
        $this->interval = $interval;
    }

    /**
     * @return boolean
     */
    public function getIgnoreInterval() : bool
    {
        return $this->ignoreInterval;
    }

    /**
     * @param mixed $ignoreInterval
     */
    public function setIgnoreInterval(bool $ignoreInterval)
    {
        $this->ignoreInterval = $ignoreInterval;
    }

    /**
     * @return bool
     */
    public function getIgnoreQuantity() : bool
    {
        return $this->ignoreQuantity;
    }

    /**
     * @param mixed $ignoreQuantity
     */
    public function setIgnoreQuantity(bool $ignoreQuantity)
    {
        $this->ignoreQuantity = $ignoreQuantity;
    }

    /**
     * @return int
     */
    public function getPrice() : int
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

}

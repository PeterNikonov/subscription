<?php

namespace PeterNikonov\Subscription;

class Contract
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $client;
    /*
     * @type Condition
     */
    private $condition;
    /*
     * @var Episode[] $episodes
     */
    private $episodes = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getClient(): int
    {
        return $this->client;
    }

    /**
     * @param int $client
     */
    public function setClient(int $client)
    {
        $this->client = $client;
    }

    /**
     * @return Condition
     */
    public function getCondition() : Condition
    {
        return $this->condition;
    }

    /**
     * @param Condition $condition
     */
    public function setCondition(Condition $condition)
    {
        $this->condition = $condition;
    }

    /**
     * @return Episode[]
     */
    public function getEpisodes() : array
    {
        return $this->episodes;
    }

    /**
     * @param mixed $episodes
     */
    public function setEpisode(Episode $episode)
    {
        $this->episodes[] = $episode;
    }
}
